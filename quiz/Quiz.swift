//
//  Quiz.swift
//  quiz
//
//  Created by Franciszek Gonciarz on 10.03.2017.
//  Copyright © 2017 fragon. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

/// Represents Quiz list response obtained from API
final class QuizListResponse: ResponseObjectSerializable, ResponseCollectionSerializable {
    
    public let count: Int?
    public let items: [Quiz]?
    
    init(count: Int?, items: [Quiz]?) {
        self.count = count
        self.items = items
    }
    
    required init?(response: HTTPURLResponse, representation: Any) {
        guard
            let representation = representation as? [String: Any]
            else {
                return nil
        }
        self.count = representation["count"] as? Int
        self.items = Quiz.collection(from:response, withRepresentation: representation["items"]!)
    }
    
}

final class Quiz: Object, ResponseObjectSerializable, ResponseCollectionSerializable {
    
    let id = RealmOptional<Int>()
    let questions = List<Question>()
    dynamic var title: String?
    dynamic var type: String?
    dynamic var mainPhoto: Image?
    
    dynamic var state: State?
    
    convenience init?(response: HTTPURLResponse, representation: Any) {
        guard
            let representation = representation as? [String: Any]
            else {
                return nil
        }
        self.init()
        self.id.value = representation["id"] as? Int
        self.questions.append(objectsIn: Question.collection(from:response, withRepresentation: representation["questions"]!))
        self.title = representation["title"] as? String
        self.type = representation["type"] as? String
        self.mainPhoto = Image(response:response, representation: representation["mainPhoto"]!)!
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

final class Question: Object, ResponseObjectSerializable, ResponseCollectionSerializable {
    
    dynamic var image : Image?
    let answers = List<Answer>()
    dynamic var text : String?
    dynamic var answer : String?
    dynamic var type : String?
    let order = RealmOptional<Int>()
    
    convenience init?(response: HTTPURLResponse, representation: Any) {
        guard
            let representation = representation as? [String: Any]
            else {
                return nil
        }
        self.init()
        self.image = Image(response:response, representation: representation["image"]!)!
        self.answers.append(objectsIn: Answer.collection(from: response, withRepresentation: representation["answers"]!))
        self.text = representation["text"] as? String
        self.answer = representation["answer"] as? String
        self.type = representation["type"] as? String
        self.order.value = representation["order"] as? Int
    }
    
}

final class Answer: Object, ResponseObjectSerializable, ResponseCollectionSerializable {
    
    dynamic var image: Image?
    let order = RealmOptional<Int>()
    dynamic var text: String?
    let isCorrect = RealmOptional<Int>()
    
    convenience init?(response: HTTPURLResponse, representation: Any) {
        guard
            let representation = representation as? [String: Any]
            else {
                return nil
        }
        self.init()
        self.image = Image(response:response, representation: representation["image"]!)!
        self.order.value = representation["order"] as? Int
        self.text = representation["text"] as? String
        if self.text == nil {
            let textInt = representation["text"] as? Int
            if let textInt = textInt {
                self.text = String(textInt)
            }
        }
        self.isCorrect.value = representation["isCorrect"] as? Int
    }
}

class State: Object {
    
    let result = RealmOptional<Int>()
    let progress = RealmOptional<Int>()
    dynamic var finished = false
    
}
