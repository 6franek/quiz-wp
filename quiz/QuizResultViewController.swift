//
//  QuizResultViewController.swift
//  quiz
//
//  Created by Franciszek Gonciarz on 13.03.2017.
//  Copyright © 2017 fragon. All rights reserved.
//

import UIKit

class QuizResultViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var solveAgainButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    
    /// Currently solved quiz id
    var quizId: Int?
    
    /// Currently solved Quiz
    var quiz: Quiz?
    
    /// Number of correct answers given by the user
    var correctAnswersNumber: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        solveAgainButton.addTarget(self, action: #selector(solveAgain(sender:)), for: .touchUpInside)
        finishButton.addTarget(self, action: #selector(finish(sender:)), for: .touchUpInside)
        if let correctAnswersNumber = correctAnswersNumber, let quiz = quiz {
            let questions = quiz.questions
            resultLabel.text = "\(String(Int(round(Float(correctAnswersNumber) / Float(questions.count) * 100))))%"
        }
    }
    
    
    /// Opens the same quiz again
    ///
    /// - Parameter sender: sender
    func solveAgain(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let quizViewController = storyboard.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
        quizViewController.quiz = quiz
        quizViewController.quizId = quiz?.id.value
        present(quizViewController, animated: true, completion: nil)
    }
    
    /// Finishes quiz and returns to the main list
    ///
    /// - Parameter sender: sender
    func finish(sender: UIButton) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }

}
