//
//  ViewController.swift
//  quiz
//
//  Created by Franciszek Gonciarz on 10.03.2017.
//  Copyright © 2017 fragon. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class QuizListViewController: UIViewController {
    
    @IBOutlet weak var quizTableView: UITableView!
    @IBOutlet weak var quizListTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var downloadActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableViewInfoLabel: UILabel!
    
    let refreshControl = UIRefreshControl()
    
    /// Quizzes which are displayed to the user
    var quizzes = [Quiz]()
    
    /// Realm instance
    var realm: Realm?
    
    /// Index of the cell which should be reloaded next time this UIViewController is shown
    var cellIndexToReload: Int?
    
    /// Current state of this UIViewController
    var viewState: ViewState = .all
    
    /// Pagination limit
    static let limit = 20
    
    /// True if there is no more content to be downloaded from API
    var downloadedAll = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadActivityIndicator.isHidden = true
        tableViewInfoLabel.isHidden = true
        configureTableView()
        configureSegmentedControl()
        loadData(refresh: false, fromBeginning: true)
        realm = try! Realm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadQuizCell()
    }
    
    /// Reloads single quiz cell
    func reloadQuizCell() {
        if let indexToReload = cellIndexToReload {
            let animationsEnabled = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            let indexPath = IndexPath(row: indexToReload, section: 0)
            quizTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            UIView.setAnimationsEnabled(animationsEnabled)
            cellIndexToReload = nil
        }
    }
    
    /// Configures quizTableView
    func configureTableView() {
        quizTableView.dataSource = self
        quizTableView.delegate = self
        quizTableView.separatorStyle = .none
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        quizTableView.addSubview(refreshControl)
    }
    
    /// Configures quizListTypeSegmentedControl
    func configureSegmentedControl() {
        quizListTypeSegmentedControl.addTarget(self, action: #selector(changeListType(sender:)), for: .valueChanged)
    }
    
    /// Changes currently displayed list type
    ///
    /// - Parameter sender: sender
    func changeListType(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            viewState = .all
            loadData(refresh: false, fromBeginning: true)
        case 1:
            viewState = .saved
            loadDataFromDatabase()
        default:
            break
        }
    }
    
    /// Refreshes UITableView's contents
    ///
    /// - Parameter sender: sender
    func refresh(sender: UIRefreshControl) {
        if viewState == .all {
            loadData(refresh: true, fromBeginning: true)
        } else {
            refreshControl.endRefreshing()
        }
    }
    
    /// Loads data from api
    ///
    /// - Parameters:
    ///   - refresh: true if initialized by swipe to refresh gesture, false otherwise
    ///   - fromBeginning: true if should download data with 0 offset, false otherwise
    func loadData(refresh: Bool, fromBeginning: Bool) {
        if fromBeginning {
            downloadedAll = false
        }
        if (!refresh && quizzes.count == 0) {
            showIndicator()
            hideInfoLabel()
        }
        var offset = 0
        if !fromBeginning {
            offset = quizzes.count
        }
        ApiManager.getQuizzes(offset: offset, limit: offset + QuizListViewController.limit) {
            (quizzes: [Quiz]?, isSuccess: Bool) in
            DispatchQueue.main.async {
                if quizzes != nil && isSuccess {
                    if quizzes!.count < QuizListViewController.limit {
                        self.downloadedAll = true
                    }
                    if fromBeginning {
                        self.quizzes = quizzes!
                    } else {
                        self.quizzes.append(contentsOf: quizzes!)
                    }
                    self.refreshControl.endRefreshing()
                    self.quizTableView.reloadData()
                    if !refresh && fromBeginning {
                        self.scrollToTheTop()
                    }
                    self.hideInfoLabel()
                } else {
                    self.showInfoLabel(message: NSLocalizedString("Nie mogę pobrać quizów. Sprawdź połączenie z Internetem i spróbuj ponownie.", comment: "No quizzes message"))
                }
                self.hideIndicator()
            }
        }
    }
    
    /// Loads data from database
    func loadDataFromDatabase() {
        if let realm = realm {
            let databaseQuizzes = realm.objects(Quiz.self)
            self.quizzes = [Quiz]()
            for databaseQuiz in databaseQuizzes {
                self.quizzes.append(databaseQuiz)
            }
            self.quizTableView.reloadData()
            scrollToTheTop()
            if self.quizzes.count == 0 {
                self.showInfoLabel(message: NSLocalizedString("Tutaj pojawią się rozwiązywane przez Ciebie quizy", comment: "No saved quizzes message"))
            } else {
                self.hideInfoLabel()
            }
        } else {
            self.showInfoLabel(message: NSLocalizedString("Tutaj pojawią się rozwiązywane przez Ciebie quizy", comment: "No saved quizzes message"))
        }
    }
    
    /// Saves data to database
    ///
    /// - Parameter quizzes: quizzes to be saved in database
    func saveDataToDatabase(quizzes: [Quiz]) {
        if let realm = realm {
            try! realm.write {
                for quiz in quizzes {
                    realm.add(quiz, update: true)
                }
            }
        }
    }
    
    /// Scrolls quizTableView to the top
    func scrollToTheTop() {
        if quizzes.count > 0 {
            let topPath = IndexPath(row: 0, section: 0)
            quizTableView.scrollToRow(at: topPath, at: .top, animated: false)
        }
    }
    
    
    /// Shows downloadActivityIndicator
    func showIndicator() {
        downloadActivityIndicator.isHidden = false
        downloadActivityIndicator.bringSubview(toFront: self.view)
        downloadActivityIndicator.startAnimating()
    }
    
    /// Hides downloadActivityIndicator
    func hideIndicator() {
        downloadActivityIndicator.stopAnimating()
        downloadActivityIndicator.hidesWhenStopped = true
        downloadActivityIndicator.isHidden = true
    }
    
    /// Shows tableViewInfoLabel
    ///
    /// - Parameter message: message to be displayed in tableViewInfoLabel
    func showInfoLabel(message: String) {
        tableViewInfoLabel.text = message
        tableViewInfoLabel.isHidden = false
    }
    
    /// Hides tableViewInfoLabel
    func hideInfoLabel() {
        tableViewInfoLabel.isHidden = true
    }
    
    /// Describes QuizListViewController state
    ///
    /// - all: when quizzes downloaded from api are displayed
    /// - saved: when quizzes saved in the local database are displayed
    enum ViewState {
        case all
        case saved
    }
    
}



extension QuizListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let quiz = quizzes[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let quizViewController = storyboard.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
        quizViewController.quiz = quiz
        quizViewController.quizId = quiz.id.value
        present(quizViewController, animated: true, completion: nil)
        cellIndexToReload = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quizzes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = quizTableView.dequeueReusableCell(withIdentifier: "QuizTableViewCell", for: indexPath) as! QuizTableViewCell
        cell.backgroundImageView.clipsToBounds = true
        if let image = quizzes[indexPath.row].mainPhoto, let path = image.url {
            cell.backgroundImageView.kf.indicatorType = .activity
            cell.backgroundImageView.kf.setImage(with: URL(string: path))
        }
        cell.titleLabel.text = quizzes[indexPath.row].title
        var quizState: State? = nil
        var questionsCount: Int? = nil
        if let realm = realm, let databaseQuiz = realm.object(ofType: Quiz.self, forPrimaryKey: quizzes[indexPath.row].id.value) {
            if let state = databaseQuiz.state {
                quizState = state
            }
            questionsCount = databaseQuiz.questions.count
        }
        if let quizState = quizState, let progress = quizState.progress.value, let questionsCount = questionsCount {
            if let result = quizState.result.value, quizState.finished {
                let percents = Int(round(Double(result) / Double(questionsCount) * 100))
                cell.resultLabel.text = "Ostatni wynik: \(result)/\(questionsCount) \(percents)%"
            } else {
                let percents = Int(round(Double(progress) / Double(questionsCount) * 100))
                cell.resultLabel.text = "Quiz rozwiązany w \(percents)%"
            }
        } else {
            cell.resultLabel.text = "Rozpocznij quiz"
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.lightGray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenWidth = tableView.frame.size.width
        let quiz = quizzes[indexPath.row]
        print("Quiz image height: \(quiz.mainPhoto!.height), width: \(quiz.mainPhoto!.width)")
        if let image = quiz.mainPhoto, let imageWidth = image.width.value, let imageHeight = image.height.value {
            let ratio = CGFloat(Double(imageHeight) / Double(imageWidth))
            return (ratio * screenWidth) + 78
        }
        return 160
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = quizzes.count - 1
        if indexPath.row == lastElement && !downloadedAll && viewState == .all {
            loadData(refresh: false, fromBeginning: false)
        }
    }
    
}
