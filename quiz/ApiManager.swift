//
//  ApiManager.swift
//  quiz
//
//  Created by Franciszek Gonciarz on 10.03.2017.
//  Copyright © 2017 fragon. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    
    /// Downloads quizzes from API
    ///
    /// - Parameters:
    ///   - offset: offset
    ///   - limit: limit
    ///   - onCompletion: completion handler
    class func getQuizzes(offset: Int, limit: Int, onCompletion: @escaping ([Quiz]?, Bool) -> Void) {
        let requestUrl = "https://quiz.o2.pl/api/v1/quizzes/\(offset)/\(limit)"
        _ = Alamofire.request(requestUrl).responseObject {
            (response: DataResponse<QuizListResponse>) in
            switch response.result {
            case .success(let quizListResponse):
                print("Quiz list response: \(quizListResponse)")
                if let quizzes = quizListResponse.items {
                    onCompletion(quizzes, true)
                } else {
                    onCompletion(nil, false)
                }
            case .failure(let error):
                print("Request failed with error: \(error)")
                onCompletion(nil, false)
            }
        }
    }
    
    /// Downloads single quiz from API
    ///
    /// - Parameters:
    ///   - quizId: ID of the quiz to be downloaded
    ///   - onCompletion: completion handler
    class func getQuiz(quizId: String, onCompletion: @escaping (Quiz?, Bool) -> Void) {
        let requestUrl = "https://quiz.o2.pl/api/v1/quiz/\(quizId)/0"
        _ = Alamofire.request(requestUrl).responseObject {
            (response: DataResponse<Quiz>) in
            switch response.result {
            case .success(let quiz):
                print("Quiz: \(quiz)")
                onCompletion(quiz, true)
            case .failure(let error):
                print("Request failed with error: \(error)")
                onCompletion(nil, false)
            }
        }
    }
    
}

protocol ResponseObjectSerializable {
    init?(response: HTTPURLResponse, representation: Any)
}

extension DataRequest {
    func responseObject<T: ResponseObjectSerializable>(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<T>) -> Void)
        -> Self
    {
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
            guard error == nil else { return .failure(BackendError.network(error: error!)) }
            
            let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = jsonResponseSerializer.serializeResponse(request, response, data, nil)
            
            guard case let .success(jsonObject) = result else {
                return .failure(BackendError.jsonSerialization(error: result.error!))
            }
            
            guard let response = response, let responseObject = T(response: response, representation: jsonObject) else {
                return .failure(BackendError.objectSerialization(reason: "JSON could not be serialized: \(jsonObject)"))
            }
            
            return .success(responseObject)
        }
        
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}

protocol ResponseCollectionSerializable {
    static func collection(from response: HTTPURLResponse, withRepresentation representation: Any) -> [Self]
}

extension ResponseCollectionSerializable where Self: ResponseObjectSerializable {
    static func collection(from response: HTTPURLResponse, withRepresentation representation: Any) -> [Self] {
        var collection: [Self] = []
        
        if let representation = representation as? [[String: Any]] {
            for itemRepresentation in representation {
                if let item = Self(response: response, representation: itemRepresentation) {
                    collection.append(item)
                }
            }
        }
        
        return collection
    }
}

extension DataRequest {
    @discardableResult
    func responseCollection<T: ResponseCollectionSerializable>(
        queue: DispatchQueue? = nil,
        completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<[T]> { request, response, data, error in
            guard error == nil else { return .failure(BackendError.network(error: error!)) }
            
            let jsonSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = jsonSerializer.serializeResponse(request, response, data, nil)
            
            guard case let .success(jsonObject) = result else {
                return .failure(BackendError.jsonSerialization(error: result.error!))
            }
            
            guard let response = response else {
                let reason = "Response collection could not be serialized due to nil response."
                return .failure(BackendError.objectSerialization(reason: reason))
            }
            
            return .success(T.collection(from: response, withRepresentation: jsonObject))
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}

enum BackendError: Error {
    case network(error: Error)
    case dataSerialization(error: Error)
    case jsonSerialization(error: Error)
    case xmlSerialization(error: Error)
    case objectSerialization(reason: String)
}
