//
//  Image.swift
//  quiz
//
//  Created by Franciszek Gonciarz on 10.03.2017.
//  Copyright © 2017 fragon. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

/// Represents API Image object
final class Image: Object, ResponseObjectSerializable, ResponseCollectionSerializable {
    
    dynamic var author: String?
    let width = RealmOptional<Int>()
    dynamic var mediaId: String?
    dynamic var source: String?
    dynamic var url: String?
    let height = RealmOptional<Int>()
    
    convenience init?(response: HTTPURLResponse, representation: Any) {
        guard
            let representation = representation as? [String: Any]
            else {
                return nil
        }
        self.init()
        self.author = representation["author"] as? String
        self.width.value = representation["width"] as? Int
        self.mediaId = representation["mediaId"] as? String
        self.source = representation["source"] as? String
        self.url = representation["url"] as? String
        self.height.value = representation["height"] as? Int
    }
    
}
