//
//  QuizViewController.swift
//  quiz
//
//  Created by Franciszek Gonciarz on 12.03.2017.
//  Copyright © 2017 fragon. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class QuizViewController: UIViewController {
    
    @IBOutlet weak var quizProgressView: UIProgressView!
    @IBOutlet weak var quizProgressLabel: UILabel!
    @IBOutlet weak var questionImageView: UIImageView!
    @IBOutlet weak var questionImageViewRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answersTableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    /// Currently solved quiz id
    var quizId: Int?
    
    /// Currently solved Quiz
    var quiz: Quiz?
    
    /// Questions of the currently solved Quiz
    var questions = List<Question>()
    
    /// Currently displayed question
    var currentQuestion: Question?
    
    /// Currently displayed answers
    var currentAnswers = List<Answer>()
    
    /// Index of the currently displayed question
    var currentQuestionIndex = 0
    
    /// Number of correct answers given by the user
    var correctAnswersNumber = 0
    
    /// Realm instance
    var realm: Realm?

    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        configureTableView()
        configureTopBar()
        loadQuiz()
    }
    
    /// Configures tableViewInfoLabel
    func configureTableView() {
        answersTableView.delegate = self
        answersTableView.dataSource = self
        answersTableView.estimatedRowHeight = 145
        answersTableView.rowHeight = UITableViewAutomaticDimension
        answersTableView.tableFooterView = UIView()
        answersTableView.separatorStyle = .none
    }
    
    /// Configures top bar
    func configureTopBar() {
        closeButton.addTarget(self, action: #selector(closeQuiz(sender:)), for: .touchUpInside)
    }
    
    /// Closes this UIViewController
    ///
    /// - Parameter sender: sender
    func closeQuiz(sender: UIButton) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    /// Loads quiz based on quizId value
    func loadQuiz() {
        if let quizId = quizId {
            if let realm = realm, let savedQuiz = realm.object(ofType: Quiz.self, forPrimaryKey: quizId) {
                retrieveQuizFromDatabase(savedQuiz: savedQuiz)
            } else {
                ApiManager.getQuiz(quizId: String(quizId)) {
                    (quiz: Quiz?, isSuccess: Bool) in
                    DispatchQueue.main.async {
                        if quiz != nil && isSuccess {
                            self.quiz = quiz
                            if let quiz = quiz {
                                self.quiz = quiz
                                self.questions = quiz.questions
                                self.loadQuestion(questionIndex: self.currentQuestionIndex)
                                self.titleLabel.text = quiz.title
                                self.saveQuizToDatabase()
                            }
                        } else if let realm = self.realm, let savedQuiz = realm.object(ofType: Quiz.self, forPrimaryKey: quizId) {
                            self.retrieveQuizFromDatabase(savedQuiz: savedQuiz)
                        }
                    }
                }
            }
        }
    }
    
    /// Retrieves quiz from the database
    ///
    /// - Parameter savedQuiz: quiz returned from realm
    func retrieveQuizFromDatabase(savedQuiz: Quiz) {
        quiz = savedQuiz
        questions = savedQuiz.questions
        if let quiz = quiz {
            titleLabel.text = quiz.title
        }
        if let state = savedQuiz.state, let progress = state.progress.value, let result = state.result.value, !state.finished {
            currentQuestionIndex = progress
            correctAnswersNumber = result
            loadQuestion(questionIndex: progress)
            return
        }
        self.loadQuestion(questionIndex: 0)
    }
    
    /// Saves quiz to database
    func saveQuizToDatabase() {
        saveQuizToDatabase(quizFinished: false)
    }
    
    /// Saves quiz to database
    ///
    /// - Parameter quizFinished: true if the quiz has been finished by the user
    func saveQuizToDatabase(quizFinished: Bool) {
        if let realm = realm {
            try! realm.write {
                if let quiz = quiz {
                    let state = State()
                    state.progress.value = currentQuestionIndex
                    state.result.value = correctAnswersNumber
                    state.finished = quizFinished
                    if let savedQuiz = realm.object(ofType: Quiz.self, forPrimaryKey: quiz.id.value) {
                        if let oldState = savedQuiz.state {
                            realm.delete(oldState)
                        }
                        savedQuiz.state = state
                        realm.add(savedQuiz, update: true)
                    } else {
                        quiz.state = state
                        realm.add(quiz, update: true)
                    }
                }
            }
        }
    }
    
    /// Loads question
    ///
    /// - Parameter questionIndex: index of the question to be loaded
    func loadQuestion(questionIndex: Int) {
        let question = questions[questionIndex]
        currentQuestion = question
        currentAnswers = question.answers
        questionLabel.text = question.text
        if let questionImage = question.image, let path = questionImage.url, !path.isEmpty {
            questionImageView.frame = CGRect(x: 0, y: 0, width: questionImageView.frame.width, height: 0)
            if let ratioConstraint = questionImageViewRatioConstraint {
                ratioConstraint.isActive = true
            }
            questionImageView.isHidden = false
            self.view.layoutSubviews()
            questionImageView.kf.indicatorType = .activity
            questionImageView.kf.setImage(with: URL(string: path))
        } else {
            questionImageView.frame = CGRect(x: 0, y: 0, width: questionImageView.frame.width, height: 0)
            if let ratioConstraint = questionImageViewRatioConstraint {
                ratioConstraint.isActive = false
            }
            questionImageView.isHidden = true
            self.view.layoutSubviews()
        }
        quizProgressView.setProgress(Float(questionIndex) / Float(questions.count), animated: true)
        quizProgressLabel.text = "Pytanie \(questionIndex + 1)/\(questions.count)"
        answersTableView.reloadData()
    }
    
    /// Proceeds to the next question
    func moveToNextQuestion() {
        currentQuestionIndex += 1
        if currentQuestionIndex < questions.count {
            animateQuestionChange(questionIndex: currentQuestionIndex)
            saveQuizToDatabase()
        } else {
            saveQuizToDatabase(quizFinished: true)
            showSummary()
        }
    }
    
    /// Animates question change
    ///
    /// - Parameter questionIndex: Index of the question to be shown
    func animateQuestionChange(questionIndex: Int) {
        questionImageView.alpha = 1.0
        questionLabel.alpha = 1.0
        answersTableView.alpha = 1.0
        UIView.animate(withDuration: 0.25, animations: {
            self.questionImageView.alpha = 0.0
            self.questionLabel.alpha = 0.0
            self.answersTableView.alpha = 0.0
        }) { (finished) in
            self.loadQuestion(questionIndex: questionIndex)
            UIView.animate(withDuration: 0.25, animations: {
                self.questionImageView.alpha = 1.0
                self.questionLabel.alpha = 1.0
                self.answersTableView.alpha = 1.0
            })
        }
    }
    
    /// Shows quiz summary after the user has finished solving the quiz
    func showSummary() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let quizResultViewController = storyboard.instantiateViewController(withIdentifier: "QuizResultViewController") as! QuizResultViewController
        quizResultViewController.quiz = quiz
        quizResultViewController.quizId = quiz?.id.value
        quizResultViewController.correctAnswersNumber = correctAnswersNumber
        present(quizResultViewController, animated: true, completion: nil)
    }

}

extension QuizViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO: accept answer
        let answer = currentAnswers[indexPath.row]
        if let isCorrect = answer.isCorrect.value, isCorrect == 1 {
            correctAnswersNumber += 1
        }
        moveToNextQuestion()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentAnswers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = answersTableView.dequeueReusableCell(withIdentifier: "AnswerTableViewCell", for: indexPath) as! AnswerTableViewCell
        let answer = currentAnswers[indexPath.row]
        cell.answerLabel.text = answer.text
        if let image = answer.image, let path = image.url {
            cell.answerImageView.kf.indicatorType = .activity
            cell.answerImageView.kf.setImage(with: URL(string: path))
        } else {
            cell.answerImageView.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
